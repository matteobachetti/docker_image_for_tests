FROM continuumio/miniconda3

MAINTAINER Matteo Bachetti <matteo@matteobachetti.it>

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt-get update -y && apt-get update -y && apt-get install -y default-jre software-properties-common wget xvfb psmisc && apt-get clean

RUN conda create -n py36 python=3.6 matplotlib scipy numpy numba astropy dask coverage h5py

RUN conda create -n py34 python=3.4 matplotlib scipy numpy numba astropy dask coverage 

RUN conda create -n py27 python=2.7 matplotlib scipy numpy numba astropy dask coverage h5py

CMD [ "/bin/bash" ]
